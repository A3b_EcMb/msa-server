﻿namespace MSA_server.MessageBroker.Messages;

public struct WebSocketMessage
{
    public WebSocketService service;
    public string data;
}