﻿using WebSocketSharp;

namespace MSA_server.MessageBroker.Messages;

public struct WebSocketCloseMessage
{
    public WebSocketService service;
    public CloseEventArgs args;
}