﻿namespace MSA_server.MessageBroker.Messages;

public struct WebSocketOpenMessage
{
    public WebSocketService service;
}