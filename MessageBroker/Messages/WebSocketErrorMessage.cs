﻿namespace MSA_server.MessageBroker.Messages;

public struct WebSocketErrorMessage
{
    public WebSocketService service;
    public WebSocketSharp.ErrorEventArgs args;
}