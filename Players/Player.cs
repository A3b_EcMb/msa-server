﻿using MSA_server.Rooms;
using NetFrame;
using NetFrame.Server;

namespace MSA_server.Players;

public class Player
{
    public int id;
    public int clientId;
    public string name;
    public Room currentRoom;
    public double timerToRequestInfo;
    
    public WebSocketService service;
    public NetFrameServer server;

    public void Send<T>(T dataframe) where T : struct, INetworkDataframe
    {
        if (clientId >= 0)
        {
            server.Send(ref dataframe, clientId);
            return;
        }

        if (service != null)
        {
            service.Send(dataframe);
            return;
        }

        throw new Exception($"Error in sending dataframe to player {id}! No netframe or web socket connection is open!");
    }

}