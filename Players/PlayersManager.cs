﻿using MSA_server.Dataframes;
using MSA_server.MessageBroker;
using MSA_server.MessageBroker.Messages;
using MSA_server.Rooms;
using NetFrame.Server;

namespace MSA_server.Players;

public class PlayersManager : IDisposable
{
    private const double TimeToRequestInfo = 0.5;
    
    public int PlayersCount => _players.Count;

    private int _id;

    private Dictionary<string, int> _playersWebSocketToIds = new();
    private Dictionary<int, int> _playersClientIdsToIds = new();
    
    private Dictionary<int, Player> _players = new();
    private List<Player> _playersToRequestInfo = new();

    private NetFrameServer _server;
    private RoomsManager _roomsManager;
    private LocalMessageBroker _messageBroker;

    public void Initialize()
    {
        Console.WriteLine("Initializing players manager...");

        _roomsManager = Server.Container.Resolve<RoomsManager>();
        _server = Server.Container.Resolve<NetFrameServer>();
        _messageBroker = Server.Container.Resolve<LocalMessageBroker>();
        
        _server.ClientConnection += OnClientConnected;
        _server.ClientDisconnect += OnClientDisconnected;
        
        _messageBroker.Subscribe<WebSocketOpenMessage>(OnWebSocketOpen);
        _messageBroker.Subscribe<WebSocketCloseMessage>(OnWebSocketClose);
        _messageBroker.Subscribe<WebSocketErrorMessage>(OnWebSocketError);
        _messageBroker.SubscribeDataframe<PlayerInfoDataframe>(ProcessPlayerInfo);

        Server.Update += Update;
        
        Console.WriteLine("Players manager initialized...");
    }

    private void Update(double deltaTime)
    {
        for (int i = _playersToRequestInfo.Count - 1; i >= 0; i--)
        {
            var player = _playersToRequestInfo[i];
            if (player.clientId < 0 && player.service == null)
            {
                _playersToRequestInfo.RemoveAt(i);
                continue;
            }
            
            player.timerToRequestInfo -= deltaTime;
            if (player.timerToRequestInfo > 0) continue;
            
            Console.WriteLine($"Sending info request to player {player.id}");
            var request = new PlayerInfoRequestDataframe();
            player.Send(request);
            _playersToRequestInfo.RemoveAt(i);
        }
    }

    public bool TryGetPlayerByClientId(int id, out Player player)
    {
        player = null;
        if (!_playersClientIdsToIds.TryGetValue(id, out int playerId))
            return false;
        
        return _players.TryGetValue(playerId, out player);
    }
    
    public bool TryGetPlayerByWebSocketId(string id, out Player player)
    {
        player = null;
        if (!_playersWebSocketToIds.TryGetValue(id, out int playerId))
            return false;
        
        return _players.TryGetValue(playerId, out player);
    }
    
    private void OnClientConnected(int clientId)
    {
        Console.WriteLine($"Client {clientId} connected from netFrame, adding player");
        if (TryGetPlayerByClientId(clientId, out _))
        {
            Console.WriteLine("Already has client with this id!");
            DisconnectNetFrameByReason(clientId, DisconnectReason.ServerError);
            return;
        }

        int playerId = _id++;
        var player = new Player
        {
            id = playerId,
            clientId = clientId,
            timerToRequestInfo = TimeToRequestInfo,
            server = _server,
        };
        
        Console.WriteLine($"Created player with id {playerId}");
        
        _playersClientIdsToIds.Add(clientId, playerId);
        _players.Add(playerId, player);
        _playersToRequestInfo.Add(player);
    }

    private void OnClientDisconnected(int clientId)
    {
        if (!TryGetPlayerByClientId(clientId, out var player))
            return;
        
        Console.WriteLine($"Player {player.id} disconnected");
        _roomsManager.OnPlayerDisconnected(player);
        _players.Remove(player.id);
        _playersClientIdsToIds.Remove(player.clientId);

        player.clientId = -1;
        if (player.timerToRequestInfo >= 0)
            _playersToRequestInfo.Remove(player);
    }
    
    private void OnWebSocketOpen(ref WebSocketOpenMessage message)
    {
        Console.WriteLine($"Opened new websocket connection with id {message.service.ID}");
        if (TryGetPlayerByWebSocketId(message.service.ID, out _))
        {
            Console.WriteLine("Already have player with this socket id!");
            DisconnectWebSocketByReason(message.service, DisconnectReason.ServerError);
            return;
        }
        
        int playerId = _id++;
        var player = new Player
        {
            id = playerId,
            clientId = -1,
            timerToRequestInfo = TimeToRequestInfo,
            service = message.service,
        };
        
        Console.WriteLine($"Created player with id {playerId}");
        
        _playersWebSocketToIds.Add(message.service.ID, playerId);
        _players.Add(playerId, player);
        _playersToRequestInfo.Add(player);
    }
    
    private void OnWebSocketClose(ref WebSocketCloseMessage message)
    {
        if (!TryGetPlayerByWebSocketId(message.service.ID, out var player))
            return;
        
        Console.WriteLine($"Player {player.id} disconnected");
        _roomsManager.OnPlayerDisconnected(player);
        _players.Remove(player.id);
        _playersWebSocketToIds.Remove(message.service.ID);

        if (player.timerToRequestInfo >= 0)
            _playersToRequestInfo.Remove(player);
    }

    private void OnWebSocketError(ref WebSocketErrorMessage message)
    {
        Console.WriteLine($"Web socket error {message.service.ID}");
        if (!TryGetPlayerByWebSocketId(message.service.ID, out var player))
            return;
        
        Console.WriteLine($"Disconnecting {player.id}");
        _roomsManager.OnPlayerDisconnected(player);
        _players.Remove(player.id);
        _playersWebSocketToIds.Remove(message.service.ID);

        if (player.timerToRequestInfo >= 0)
            _playersToRequestInfo.Remove(player);
    }

    private void ProcessPlayerInfo(ref PlayerInfoDataframe dataframe, Player player)
    {
        if (!dataframe.clientVersion.Equals(Server.CurrentVersion))
        {
            Console.WriteLine("Wrong client version!");
            DisconnectPlayerByReason(player, DisconnectReason.ClientVersion);
            _players.Remove(player.id);
            return;
        }

        foreach (var playerInfo in _players)
        {
            if (!dataframe.name.Equals(playerInfo.Value.name))
                continue;
            
            Console.WriteLine("Nickname already taken!");
            DisconnectPlayerByReason(player, DisconnectReason.ClientVersion);
            _players.Remove(player.id);
            return;
        }
        
        Console.WriteLine("Sending player info received");
        player.name = dataframe.name;
        var receivedFrame = new PlayerInfoReceivedDataframe();
        player.Send(receivedFrame);
    }
    

    private void DisconnectPlayerByReason(Player player, DisconnectReason reason)
    {
        if (player.clientId >= 0)
        {
            DisconnectNetFrameByReason(player.clientId, reason);
            return;
        }

        if (player.service != null)
        {
            DisconnectWebSocketByReason(player.service, reason);
            return;
        }

        throw new ArgumentException("Trying to disconnect player, but in doesn't have client id or websocket");
    }

    private void DisconnectNetFrameByReason(int clientId, DisconnectReason reason)
    {
        _server.DisconnectClientByReason(clientId, reason);
    }

    private void DisconnectWebSocketByReason(WebSocketService service, DisconnectReason reason)
    {
        var dataframe = new DisconnectByReasonDataframe
        {
            reason = reason
        };
        service.Send(dataframe);
        // TODO: disconnect
    }

    public void Dispose()
    {
        _server.ClientConnection -= OnClientConnected;
        _server.ClientDisconnect -= OnClientDisconnected;
        _messageBroker.UnsubscribeDataframe<PlayerInfoDataframe>(ProcessPlayerInfo);
    }
}