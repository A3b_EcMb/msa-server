﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MSA_server.Dataframes;
using MSA_server.Dataframes.InGame;
using MSA_server.MessageBroker;
using MSA_server.Players;
using MSA_server.Rooms;
using NetFrame;
using NetFrame.Server;

namespace MSA_server;

public class GameManager
{
    private LocalMessageBroker _messageBroker;
    private List<SubscriptionContainer> _subscribedInFoomDataframes;

    public void Initialize()
    {
        Console.WriteLine("Initializing game manager...");
        _messageBroker = Server.Container.Resolve<LocalMessageBroker>();
        _messageBroker.SubscribeDataframe<GameFinishedDataframe>(ProcessGameFinished);

        var inRoomDataframes = Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(t => typeof(IInRoomDataframe).IsAssignableFrom(t) && t.IsValueType)
            .ToList();

        var subscribeMethod = _messageBroker.GetType().GetMethod(nameof(_messageBroker.SubscribeDataframe));
        var resendMethod = GetType().GetMethod(nameof(SendToAnotherPlayerInRoom),
            BindingFlags.NonPublic | BindingFlags.Instance);

        _subscribedInFoomDataframes = new List<SubscriptionContainer>();

        foreach (var type in inRoomDataframes)
        {
            var resendGenericMethod = resendMethod.MakeGenericMethod(type);
            var actionType = typeof(ActionRefPlayer<>).MakeGenericType(type);
            var handler = Delegate.CreateDelegate(actionType, this, resendGenericMethod);
            var subscribeGenericMethod = subscribeMethod.MakeGenericMethod(type);
            subscribeGenericMethod.Invoke(_messageBroker, new[] { handler });
            
            _subscribedInFoomDataframes.Add(new SubscriptionContainer
            {
                dataframeType = type,
                handler = handler,
            });
        }
        Console.WriteLine("Game manager initialized!");
    }

    public void Dispose()
    {
        _messageBroker.UnsubscribeDataframe<GameFinishedDataframe>(ProcessGameFinished);

        var unsubscribeMethod = _messageBroker.GetType().GetMethod(nameof(_messageBroker.UnsubscribeDataframe));
        foreach (var container in _subscribedInFoomDataframes)
        {
            var unsubscribeGenericMethod = unsubscribeMethod.MakeGenericMethod(container.dataframeType);
            unsubscribeGenericMethod.Invoke(_messageBroker, new[] { container.handler });
        }
    }

    private void ProcessGameFinished(ref GameFinishedDataframe dataframe, Player player)
    {
        if (player.currentRoom != null)
            player.currentRoom.state = player.currentRoom.guest != null ? RoomState.Full : RoomState.Open;
        
        // Send this back, so client will react on it and show notif too
        player.Send(dataframe);

        // Change YouLeft to Leave, so first client will see notif You Left, but second - Partner Left
        if (dataframe.reason == GameFinishedReason.YouLeft)
            dataframe.reason = GameFinishedReason.Leave;
        
        SendToAnotherPlayerInRoom(ref dataframe, player);
    }

    private void SendToAnotherPlayerInRoom<T>(ref T dataframe, Player player) where T : struct, INetworkDataframe
    {
        if (player.currentRoom == null)
            return;

        if (player.currentRoom.owner == player)
        {
            if (player.currentRoom.guest != null)
            {
                player.currentRoom.guest.Send(dataframe);
            }
            else
            {
                // TODO: handle it
            }
        }
        else if (player.currentRoom.guest == player)
        {
            if (player.currentRoom.owner != null)
            {
                player.currentRoom.owner.Send(dataframe);
            }
            else
            {
                // TODO: handle it
            }
        }
    }
}