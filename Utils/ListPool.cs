﻿using System.Collections.Generic;

namespace MSA_server.Utils;

public class ListPool<T>
{
    private static ListPool<T> _shared;
    public static ListPool<T> Shared => _shared ?? (_shared = new ListPool<T>());

    private readonly Queue<List<T>> _pool = new();

    public List<T> Get()
    {
        return _pool.TryDequeue(out var list) ? list : new List<T>();
    }

    public void Return(List<T> list)
    {
        list.Clear();
        _pool.Enqueue(list);
    }
}