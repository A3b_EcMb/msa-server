using NetFrame.Server;

namespace NetFrame.ThreadSafeContainers
{
    public class ClientConnectionSafeContainer
    {
        public int NewClientId;
        public NetFrameClientOnServer NewClient;
    }
}