using NetFrame.Enums;

namespace NetFrame.ThreadSafeContainers
{
    public class ConnectedFailedSafeContainer
    {
        public ReasonServerConnectionFailed Reason;
    }
}