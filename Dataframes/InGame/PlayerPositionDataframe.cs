﻿using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes.InGame;

[JsonObject]
public struct PlayerPositionDataframe : IInRoomDataframe
{
    [JsonProperty("t")] public int tick;
    [JsonProperty("tp")] public bool teleported;
    [JsonProperty("p")] public Vector3Dataframe position;
    [JsonProperty("r")] public Vector3Dataframe rotation;

    [JsonProperty("at")] public byte animationType;
    [JsonProperty("as")] public float animationSpeed;
    
    public void Write(NetFrameWriter writer)
    {
        writer.WriteInt(tick);
        writer.WriteBool(teleported);
        writer.Write(position);
        writer.Write(rotation);
        writer.WriteByte(animationType);
        writer.WriteFloat(animationSpeed);
    }

    public void Read(NetFrameReader reader)
    {
        tick = reader.ReadInt();
        teleported = reader.ReadBool();
        position = reader.Read<Vector3Dataframe>();
        rotation = reader.Read<Vector3Dataframe>();
        animationType = reader.ReadByte();
        animationSpeed = reader.ReadFloat();
    }
}