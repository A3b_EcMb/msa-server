﻿using NetFrame;
using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes.InGame;

[JsonObject]
public struct Vector3Dataframe : INetworkDataframe
{
    [JsonProperty("x")] public float x;
    [JsonProperty("y")] public float y;
    [JsonProperty("z")] public float z;
    
    public void Write(NetFrameWriter writer)
    {
        writer.WriteFloat(x);
        writer.WriteFloat(y);
        writer.WriteFloat(z);
    }

    public void Read(NetFrameReader reader)
    {
        x = reader.ReadFloat();
        y = reader.ReadFloat();
        z = reader.ReadFloat();
    }
}