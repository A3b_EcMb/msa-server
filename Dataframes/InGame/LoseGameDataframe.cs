﻿using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes.InGame;

[JsonObject]
public struct LoseGameDataframe : IInRoomDataframe
{
    [JsonProperty("r")] public byte reason;
        
    public void Write(NetFrameWriter writer)
    {
        writer.WriteByte(reason);
    }

    public void Read(NetFrameReader reader)
    {
        reason = reader.ReadByte();
    }
}