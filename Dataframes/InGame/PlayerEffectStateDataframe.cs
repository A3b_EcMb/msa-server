﻿using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes.InGame;

[JsonObject]
public struct PlayerEffectStateDataframe : IInRoomDataframe
{
    [JsonProperty("t")] public byte type;
    [JsonProperty("a")] public bool active;
        
    public void Write(NetFrameWriter writer)
    {
        writer.WriteByte(type);
        writer.WriteBool(active);
    }

    public void Read(NetFrameReader reader)
    {
        type = reader.ReadByte();
        active = reader.ReadBool();
    }
}