﻿using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes.InGame;

[JsonObject]
public struct SetCurrentTickDataframe : IInRoomDataframe
{
    [JsonProperty("t")] public int tick;
        
    public void Write(NetFrameWriter writer)
    {
        writer.WriteInt(tick);
    }

    public void Read(NetFrameReader reader)
    {
        tick = reader.ReadInt();
    }
}