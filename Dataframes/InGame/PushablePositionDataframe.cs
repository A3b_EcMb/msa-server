﻿using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes.InGame;

[JsonObject]
public struct PushablePositionDataframe : IInRoomDataframe
{
    [JsonProperty("s")] public Vector3Dataframe startPosition;
    [JsonProperty("p")] public Vector3Dataframe position;
    [JsonProperty("r")] public Vector3Dataframe rotation;
    
    public void Write(NetFrameWriter writer)
    {
        writer.Write(startPosition);
        writer.Write(position);
        writer.Write(rotation);
    }

    public void Read(NetFrameReader reader)
    {
        startPosition = reader.Read<Vector3Dataframe>();
        position = reader.Read<Vector3Dataframe>();
        rotation = reader.Read<Vector3Dataframe>();
    }
}