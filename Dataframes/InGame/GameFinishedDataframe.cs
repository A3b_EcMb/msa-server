﻿using NetFrame.WriteAndRead;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MSA_server.Dataframes.InGame;

public enum GameFinishedReason : byte
{
    Win,
    Lose,
    Leave,
    YouLeft,
}

[JsonObject]
public struct GameFinishedDataframe : IInRoomDataframe
{
    [JsonProperty("r")] [JsonConverter(typeof(StringEnumConverter))] public GameFinishedReason reason;
    
    public void Write(NetFrameWriter writer)
    {
        writer.WriteByte((byte)reason);
    }

    public void Read(NetFrameReader reader)
    {
        reason = (GameFinishedReason) reader.ReadByte();
    }
}