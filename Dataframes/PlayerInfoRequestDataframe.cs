﻿using NetFrame;
using NetFrame.WriteAndRead;
using Newtonsoft.Json;

namespace MSA_server.Dataframes;

[JsonObject]
public struct PlayerInfoRequestDataframe : INetworkDataframe
{
    public void Write(NetFrameWriter writer)
    {
    }

    public void Read(NetFrameReader reader)
    {
    }
}