﻿using MSA_server.Rooms;
using NetFrame;
using NetFrame.WriteAndRead;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MSA_server.Dataframes.Rooms;

[JsonObject]
public struct JoinRoomFailedDataframe : INetworkDataframe
{
    [JsonProperty("r")] [JsonConverter(typeof(StringEnumConverter))] public JoinRoomFailedReason reason;
        
    public void Write(NetFrameWriter writer)
    {
        writer.WriteByte((byte)reason);
    }

    public void Read(NetFrameReader reader)
    {
        reason = (JoinRoomFailedReason)reader.ReadByte();
    }
}