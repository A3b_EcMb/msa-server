using MSA_server.MessageBroker;
using MSA_server.Players;

namespace MSA_server;

public interface IDataframeWrapper
{
    public void Trigger(LocalMessageBroker broker, Player id);
}