using MSA_server.MessageBroker;
using MSA_server.Players;
using NetFrame;
using Newtonsoft.Json;

namespace MSA_server;

[JsonObject]
public class DataframeWrapper<T> : IDataframeWrapper where T : struct, INetworkDataframe
{
    [JsonProperty("d")] public T dataframe;
    
    public void Trigger(LocalMessageBroker broker, Player id) => broker.TriggerWithPlayer(ref dataframe, id);
}