using Newtonsoft.Json;

namespace MSA_server;

[JsonObject]
public struct DataframeContainer
{
    [JsonProperty("t")] public string dataframeType;
    [JsonProperty("c")] public string contents;
}