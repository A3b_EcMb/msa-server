﻿using MSA_server.MessageBroker;
using MSA_server.MessageBroker.Messages;
using NetFrame;
using Newtonsoft.Json;
using WebSocketSharp;
using WebSocketSharp.Server;
using ErrorEventArgs = WebSocketSharp.ErrorEventArgs;

namespace MSA_server;

public class WebSocketService : WebSocketBehavior
{
    private LocalMessageBroker _messageBroker;

    public void Send<T>(T dataframe) where T : struct, INetworkDataframe
    {
        var type = typeof(T);
        var wrapper = new DataframeWrapper<T>
        {
            dataframe = dataframe
        };
        string contents = JsonConvert.SerializeObject(wrapper);
        var container = new DataframeContainer
        {
            contents = contents,
            dataframeType = type.Name
        };
        string json = JsonConvert.SerializeObject(container);
        base.Send(json);
    }
    
    protected override void OnOpen()
    {
        Console.WriteLine($"Websocket {ID} open!");
        _messageBroker = Server.Container.Resolve<LocalMessageBroker>();
        var message = new WebSocketOpenMessage
        {
            service = this
        };
        _messageBroker.Trigger(ref message);
    }

    protected override void OnClose(CloseEventArgs e)
    {
        Console.WriteLine($"Websocket {ID} close! {e.Reason}");
        var message = new WebSocketCloseMessage
        {
            service = this,
            args = e,
        };
        _messageBroker.Trigger(ref message);
    }

    protected override void OnError(ErrorEventArgs e)
    {
        Console.WriteLine($"Websocket {ID} Error: {e.Message}");
        var message = new WebSocketErrorMessage
        {
            service = this,
            args = e,
        };
        _messageBroker.Trigger(ref message);
    }

    protected override void OnMessage(MessageEventArgs e)
    {
        Console.WriteLine($"Websocket {ID} new message: {e.Data}");
        var message = new WebSocketMessage
        {
            service = this,
            data = e.Data
        };
        _messageBroker.Trigger(ref message);
    }
}