﻿using System.Diagnostics;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using MSA_server.Dataframes;
using MSA_server.DI;
using MSA_server.MessageBroker;
using MSA_server.MessageBroker.Messages;
using MSA_server.Players;
using MSA_server.Rooms;
using NetFrame;
using NetFrame.Server;
using NetFrame.Utils;
using Newtonsoft.Json;
using WebSocketSharp.Server;

namespace MSA_server;

public class Server : IDisposable
{
    public const string CurrentVersion = "0.1.0";
    
    public static Container Container { get; private set; }
    
    private const int NetFramePort = 8080;
    private const int WebSocketPort = 8090;
    private const int MaxConnections = 100;
    private const int MinUpdateMs = 1;

    public static event Action<double> Update;
    public static event Action UpdateOneSecond;
    
    private readonly NetFrameServer _server;
    
    private readonly RoomsManager _roomsManager;
    private readonly PlayersManager _playersManager;
    private readonly GameManager _gameManager;
    
    private LocalMessageBroker _messageBroker;
    private WebSocketServer _socketServer;
    
    private List<SubscriptionContainer> _subscribedDataframes;

    public Server()
    {
        NetFrameDataframeCollection.Initialize(Assembly.GetExecutingAssembly());
        
        Container = new Container();
        Container.Register(this);
        
        _server = new NetFrameServer();
        _roomsManager = new RoomsManager();
        _playersManager = new PlayersManager();
        _gameManager = new GameManager();
        _messageBroker = new LocalMessageBroker();
        _messageBroker.Subscribe<WebSocketMessage>(OnMessage);
        
        Container.Register(_server);
        Container.Register(_roomsManager);
        Container.Register(_playersManager);
        Container.Register(_gameManager);
        Container.Register(_messageBroker);
        
        Console.WriteLine("Server created!");
    }

    public async Task Start()
    {
        Console.WriteLine("Initializing server...");
        _roomsManager.Initialize();
        _playersManager.Initialize();
        _gameManager.Initialize();
        
        Console.WriteLine("Starting NetFrame server...");
        _server.Start(NetFramePort, MaxConnections);
        SubscribeRedirect();
        Console.WriteLine("Server started, listening!");
        
        Console.WriteLine("Starting websocket server...");
        _socketServer = new WebSocketServer(WebSocketPort);

        // var certificate = new X509Certificate2("certificate.pfx", "33032576");
        // _socketServer.SslConfiguration.ServerCertificate = certificate;
        
        _socketServer.AddWebSocketService<WebSocketService>("/");
        _socketServer.Start();
        Console.WriteLine("Websocked server started!");

        var stopwatch = new Stopwatch();
        double updateOneSecondTimer = 0;

        while (true)
        {
            stopwatch.Stop();
            double elapsed = stopwatch.Elapsed.TotalMilliseconds;
            stopwatch.Restart();

            try
            {
                _server.Run();
                Update?.Invoke(elapsed);
                updateOneSecondTimer += elapsed;
                if (updateOneSecondTimer > 1000)
                {
                    UpdateOneSecond?.Invoke();
                    updateOneSecondTimer = 0;
                }

                // if (elapsed < MinUpdateMs)
                //     await Task.Delay(MinUpdateMs);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception in server update: {e}");
            }
        }
    }

    private void SubscribeRedirect()
    {
        var dataframes = Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(t => typeof(INetworkDataframe).IsAssignableFrom(t) && t.IsValueType)
            .ToList();

        var subscribeMethod = _server.GetType().GetMethod(nameof(_server.Subscribe));
        var resendMethod = GetType().GetMethod(nameof(RedirectToMessageBroker),
            BindingFlags.NonPublic | BindingFlags.Instance);

        _subscribedDataframes = new List<SubscriptionContainer>();

        foreach (var type in dataframes)
        {
            var resendGenericMethod = resendMethod.MakeGenericMethod(type);
            var actionType = typeof(Action<,>).MakeGenericType(type, typeof(int));
            var handler = Delegate.CreateDelegate(actionType, this, resendGenericMethod);
            var subscribeGenericMethod = subscribeMethod.MakeGenericMethod(type);
            subscribeGenericMethod.Invoke(_server, new[] { handler });
            
            _subscribedDataframes.Add(new SubscriptionContainer
            {
                dataframeType = type,
                handler = handler,
            });
        }
    }

    private void UnsubscribeRedirect()
    {
        var unsubscribeMethod = _server.GetType().GetMethod(nameof(_server.Unsubscribe));
        foreach (var container in _subscribedDataframes)
        {
            var unsubscribeGenericMethod = unsubscribeMethod.MakeGenericMethod(container.dataframeType);
            unsubscribeGenericMethod.Invoke(_server, new[] { container.handler });
        }
    }

    private void RedirectToMessageBroker<T>(T dataframe, int clientId) where T : struct, INetworkDataframe
    {
        if (!_playersManager.TryGetPlayerByClientId(clientId, out var player))
        {
            Console.WriteLine("Cannot redirect dataframe - player not found!");
            return;
        }
        
        _messageBroker.TriggerWithPlayer(ref dataframe, player);
    }

    private void OnMessage(ref WebSocketMessage message)
    {
        var container = JsonConvert.DeserializeObject<DataframeContainer>(message.data);
        Console.WriteLine($"Deserialized container, type: {container.dataframeType}");
        var dataframeType = NetFrameDataframeCollection.GetByKey(container.dataframeType).GetType();
        Console.WriteLine($"Found dataframe type: {dataframeType.Name}");
        var wrapperType = typeof(DataframeWrapper<>).MakeGenericType(dataframeType);
        Console.WriteLine($"Got generic wrapper: {wrapperType.Name}");
        var wrapper = (IDataframeWrapper)JsonConvert.DeserializeObject(container.contents, wrapperType)!;
        
        if (!_playersManager.TryGetPlayerByWebSocketId(message.service.ID, out var player))
            return;
        
        wrapper.Trigger(_messageBroker, player);
    }

    public void Dispose()
    {
        Console.WriteLine("Disposing server!");
        UnsubscribeRedirect();
        _messageBroker.Unsubscribe<WebSocketMessage>(OnMessage);
        _server.Stop();
        _roomsManager.Dispose();
        _playersManager.Dispose();
        _gameManager.Dispose();
        _socketServer.Stop();
    }
}