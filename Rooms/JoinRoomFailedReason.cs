﻿namespace MSA_server.Rooms;

public enum JoinRoomFailedReason : byte
{
    WrongPassword,
    RoomWasClosed,
    RoomAlreadyFull,
}