﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSA_server.Dataframes;
using MSA_server.Dataframes.Rooms;
using MSA_server.MessageBroker;
using MSA_server.Players;
using NetFrame.Server;

namespace MSA_server.Rooms;

public class RoomsManager : IDisposable
{
    private int _roomId;
    public List<Room> rooms = new();

    private LocalMessageBroker _messageBroker;
    private PlayersManager _playersManager;

    public void Initialize()
    {
        Console.WriteLine("Initializing room manager...");
        _messageBroker = Server.Container.Resolve<LocalMessageBroker>();
        _messageBroker.SubscribeDataframe<RoomsRequestDataframe>(ProcessRoomsRequest);
        _messageBroker.SubscribeDataframe<CreateRoomDataframe>(ProcessCreateRoom);
        _messageBroker.SubscribeDataframe<JoinRoomRequestDataframe>(ProcessJoinRoom);
        _messageBroker.SubscribeDataframe<PlayerReadyStateDataframe>(ProcessPlayerReadyState);
        _messageBroker.SubscribeDataframe<LeaveRoomDataframe>(ProcessLeaveRoom);

        _playersManager = Server.Container.Resolve<PlayersManager>();
        Console.WriteLine("Room manager initialized!");
    }

    public void OnPlayerDisconnected(Player player)
    {
        LeaveCurrentRoom(player);
    }

    private void ProcessRoomsRequest(ref RoomsRequestDataframe dataframe, Player player)
    {
        var response = new RoomsListDataframe
        {
            onlinePlayers = _playersManager.PlayersCount,
            rooms = new List<RoomInfoDataframe>()
        };
        
        foreach (var room in rooms)
        {
            if (room.guest == null)
                response.rooms.Add(room.GetInfoDataframe());
        }
        
        player.Send(response);
    }

    private void ProcessCreateRoom(ref CreateRoomDataframe dataframe, Player player)
    {
        Console.WriteLine($"Creating room, id: {_roomId}, owner: {player.clientId}, name: {dataframe.name}, password: {dataframe.password}");
        var room = new Room
        {
            owner = player,
            name = dataframe.name,
            password = dataframe.password,
            id = _roomId++,
        };
        rooms.Add(room);

        Console.WriteLine($"Player {player.id} joining room {room.id}");
        player.currentRoom = room;
        var joinedRoomDataframe = new JoinedRoomDataframe
        {
            roomInfo = room.GetInfoDataframe()
        };
        player.Send(joinedRoomDataframe);
    }

    private void ProcessJoinRoom(ref JoinRoomRequestDataframe dataframe, Player player)
    {
        Console.WriteLine($"Join room request, player {player.id}, room id {dataframe.roomId}, password {dataframe.password}");
        int roomId = dataframe.roomId;
        var room = rooms.FirstOrDefault(x => x.id == roomId);
        if (room == null)
        {
            Console.WriteLine("Cannot find room with this id");
            var failedDataframe = new JoinRoomFailedDataframe
            {
                reason = JoinRoomFailedReason.RoomWasClosed,
            };
            player.Send(failedDataframe);
            return;
        }

        if (room.guest == player || room.owner == player)
        {
            Console.WriteLine("Wrong room request, player already in room");
            return;
        }
        
        if (room.state == RoomState.Full)
        {
            Console.WriteLine("Room already full");
            var failedDataframe = new JoinRoomFailedDataframe
            {
                reason = JoinRoomFailedReason.RoomAlreadyFull,
            };
            player.Send(failedDataframe);
            return;
        }

        if (!string.IsNullOrWhiteSpace(room.password) && !room.password.Equals(dataframe.password))
        {
            Console.WriteLine($"Wrong password for this room, needed: {room.password}, entered: {dataframe.password}");
            var failedDataframe = new JoinRoomFailedDataframe
            {
                reason = JoinRoomFailedReason.WrongPassword,
            };
            player.Send(failedDataframe);
            return;
        }

        Console.WriteLine($"Player {player.clientId} joined room {room.id}");
        room.state = RoomState.Full;
        room.guest = player;
        player.currentRoom = room;
        var joinedDataframe = new JoinedRoomDataframe
        {
            roomInfo = room.GetInfoDataframe(),
        };
        player.Send(joinedDataframe);

        var playerJoinedDataframe = new PlayerJoinedRoomDataframe
        {
            playerId = 1,
            playerName = player.name
        };
        room.owner.Send(playerJoinedDataframe);
    }

    private void ProcessPlayerReadyState(ref PlayerReadyStateDataframe dataframe, Player player)
    {
        if (player.currentRoom == null) return;
        if (player.currentRoom.state == RoomState.GameStarting)
            return;

        Console.WriteLine($"Player {player.id} changed its ready state to {dataframe.ready}");
        if (player == player.currentRoom.owner)
        {
            player.currentRoom.player1Ready = dataframe.ready;
            dataframe.playerId = 0;
            
            if (player.currentRoom.guest != null)
                player.currentRoom.guest.Send(dataframe);
        }
        else if (player == player.currentRoom.guest)
        {
            player.currentRoom.player2Ready = dataframe.ready;
            dataframe.playerId = 1;
            player.currentRoom.owner.Send(dataframe);
        }
        else
        {
            // TODO: handle it
        }
        
        player.Send(dataframe);
        if (!player.currentRoom.player1Ready || !player.currentRoom.player2Ready)
            return;

        player.currentRoom.state = RoomState.GameStarting;
        var prepareDataframe = new RoomPrepareToPlayDataframe();
        player.currentRoom.guest.Send(prepareDataframe);

        prepareDataframe.isMasterClient = true;
        player.currentRoom.owner.Send(prepareDataframe);

        player.currentRoom.player1Ready = false;
        player.currentRoom.player2Ready = false;
    }

    private void ProcessLeaveRoom(ref LeaveRoomDataframe dataframe, Player player)
    {
        LeaveCurrentRoom(player);
    }

    private void LeaveCurrentRoom(Player player)
    {
        if (player.currentRoom == null) return;

        Console.WriteLine($"Player {player.clientId} left room {player.currentRoom.id}");
        if (player == player.currentRoom.guest)
        {
            Console.WriteLine("Player was guest");
            player.currentRoom.guest = null;
            player.currentRoom.player2Ready = false;
            player.currentRoom.state = RoomState.Open;
            
            var leftDataframe = new PlayerLeftRoomDataframe();
            player.currentRoom.owner.Send(leftDataframe);
        }
        else if (player == player.currentRoom.owner)
        {
            Console.WriteLine("Player was owner");
            if (player.currentRoom.guest == null)
            {
                Console.WriteLine("Room empty, destroying");
                rooms.Remove(player.currentRoom);
            }
            else
            {
                Console.WriteLine("Switching room owner");
                player.currentRoom.state = RoomState.Open;
                player.currentRoom.owner = player.currentRoom.guest;
                player.currentRoom.guest = null;
                player.currentRoom.player1Ready = false;
                player.currentRoom.player2Ready = false;

                var leftDataframe = new PlayerLeftRoomDataframe();
                player.currentRoom.owner.Send(leftDataframe);
                
                var joinedDataframe = new JoinedRoomDataframe
                {
                    roomInfo = player.currentRoom.GetInfoDataframe()
                };
                player.currentRoom.owner.Send(joinedDataframe);
            }
        }

        player.currentRoom = null;
    }

    public void Dispose()
    {
        _messageBroker.SubscribeDataframe<RoomsRequestDataframe>(ProcessRoomsRequest);
        _messageBroker.SubscribeDataframe<CreateRoomDataframe>(ProcessCreateRoom);
        _messageBroker.SubscribeDataframe<JoinRoomRequestDataframe>(ProcessJoinRoom);
        _messageBroker.SubscribeDataframe<PlayerReadyStateDataframe>(ProcessPlayerReadyState);
        _messageBroker.SubscribeDataframe<LeaveRoomDataframe>(ProcessLeaveRoom);
    }
}