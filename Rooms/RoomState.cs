﻿namespace MSA_server.Rooms;

public enum RoomState
{
    Open,
    Full,
    GameStarting,
    InGame,
}