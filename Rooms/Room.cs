﻿using MSA_server.Dataframes;
using MSA_server.Dataframes.Rooms;
using MSA_server.Players;

namespace MSA_server.Rooms;

public class Room
{
    public int id;
    public Player owner;
    public Player guest;
    public string password;
    public string name;
    public RoomState state;
    public bool player1Ready;
    public bool player2Ready;

    public RoomInfoDataframe GetInfoDataframe()
    {
        return new RoomInfoDataframe
        {
            ownerName = owner.name,
            guestName = guest != null ? guest.name : "",
            name = name,
            roomId = id,
            player1Ready = player1Ready,
            player2Ready = player2Ready,
            hasPassword = !string.IsNullOrWhiteSpace(password),
        };
    }
}